using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    //private CharacterController controller;
    private Rigidbody rb;
    [SerializeField]private float playerSpeed = 5.0f;
    private bool isCanMove =true;


    private void Start()
    {
        //controller = gameObject.AddComponent<CharacterController>();
        rb= gameObject.GetComponent<Rigidbody>();
    }

    void Update()
    {
        if(isCanMove)
        {
        if (Input.GetAxisRaw("Horizontal") > 0.5f || Input.GetAxisRaw("Horizontal") < -0.5f)
        {
            //transform.Translate(Vector3.right * Input.GetAxis("Horizontal") * playerSpeed);
            rb.velocity += transform.right * Input.GetAxisRaw("Horizontal") * playerSpeed * Time.deltaTime;
        }
        if (Input.GetAxisRaw("Vertical") > 0.5f || Input.GetAxisRaw("Vertical") < -0.5f)
        {
            //transform.Translate(Vector3.forward * Input.GetAxis("Vertical") * playerSpeed);
            rb.velocity += transform.forward * Input.GetAxisRaw("Vertical") * playerSpeed * Time.deltaTime;
        }
        }

    }

    public void StopMove()
    {
        if(isCanMove) { isCanMove= false;}
    }
}
