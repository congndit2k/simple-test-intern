
using System;
using System.Collections.Generic;
using UnityEngine;

public class SpawnerManager : MonoBehaviour
{

    [SerializeField] GameObject m_PointPrefab;
    [SerializeField] List<Material> m_Materials;


    private void OnEnable()
    {
        Player.PointUpdated += OnPointUpdated;
    }

    private void Awake()
    {
        DpawnerNewpoint(Point.PointColor.Blue);
        DpawnerNewpoint(Point.PointColor.Red);
        DpawnerNewpoint(Point.PointColor.Green);
    }

    private void OnDisable()
    {
        Player.PointUpdated -= OnPointUpdated;

    }

    void OnPointUpdated(Player.PlayerData data)
    {
        Point[] points = FindObjectsOfType<Point>();
        for (int i = 0;i < points.Length; i++)
        {
            Destroy(points[i].gameObject);
        }
        DpawnerNewpoint(Point.PointColor.Blue);
        DpawnerNewpoint(Point.PointColor.Red);
        DpawnerNewpoint(Point.PointColor.Green);
    }


    private void DpawnerNewpoint(Point.PointColor color)
    {

        GameObject point = Instantiate(m_PointPrefab, new Vector3(UnityEngine.Random.Range(-20f,20f),1, UnityEngine.Random.Range(-20f, 20f)), new Quaternion());
        point.gameObject.GetComponent<Point>().m_data.color = color;
        point.gameObject.GetComponent<MeshRenderer>().material = m_Materials[(int)color];
    }

}
