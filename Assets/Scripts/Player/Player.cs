using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using UnityEngine;

[RequireComponent(typeof(PlayerController))]
public class Player : MonoBehaviour
{

    [SerializeField]
    public struct PlayerData
    {
        public int point;
        public Point.PointColor color;
    }
    [SerializeField] List<Material> m_Materials;

    private PlayerController m_PlayerController;

    public static event Action<PlayerData> PointUpdated;
    public static event Action<bool> PlayerDie;

    [SerializeField]private PlayerData m_data;


    private void Awake()
    {
        m_PlayerController = GetComponent<PlayerController>();
    }

    private void Start()
    {
        GetRandomColor();
        PointUpdated?.Invoke(m_data);
    }

    public void UpdatePoint()
    {
        m_data.point = m_data.point + 1;
        GetRandomColor();
        PointUpdated?.Invoke(m_data);
    }


    private void GetRandomColor()
    {
        System.Random random = new System.Random(); 
        m_data.color = (Point.PointColor)random.Next(0, Enum.GetValues(typeof(Point.PointColor)).Length);
        gameObject.GetComponent<MeshRenderer>().material = m_Materials[(int)m_data.color];
    }


    private void OnTriggerEnter(Collider other)
    {
        Point point = other.gameObject.GetComponent<Point>();
        //Debug.Log(Enum.GetName(typeof(Point.PointColor), m_data.color) + " : " + Enum.GetName(typeof(Point.PointColor), point.m_data.color));

        if (point != null)
        {
            if(m_data.color == point.m_data.color)
            {
                UpdatePoint();
            }
            else
            {
                PlayerDie?.Invoke(true);
            }
        }
    }
}
