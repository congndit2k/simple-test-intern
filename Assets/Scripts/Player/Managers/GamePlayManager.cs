using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml.Serialization;
using UnityEngine;
using UnityEngine.Events;
using static Player;

public class GamePlayManager : MonoBehaviour
{
    private static GamePlayManager m_instance;

    [SerializeField]private int m_WonPoint = 10;

    public  UnityEvent GameOverEvent;
    public  UnityEvent WonEvent;

    public static GamePlayManager Instance()
    {
        if (m_instance == null)
        {
            m_instance = FindObjectOfType<GamePlayManager>();
        }
        return m_instance;
    }

    private void OnEnable()
    {
        Player.PointUpdated += OnPointUpdated;
        Player.PlayerDie += GameOver;
    }

    private void OnDisable()
    {
        Player.PointUpdated -= OnPointUpdated;
        Player.PlayerDie -= GameOver;

    }

    private void GameOver(bool isGameover)
    {
        GameOverEvent.Invoke();
    }

    private void Won()
    {
        WonEvent.Invoke();
    }


    void OnPointUpdated(Player.PlayerData data)
    {
        if(data.point == m_WonPoint)
            Won();
    }


}
