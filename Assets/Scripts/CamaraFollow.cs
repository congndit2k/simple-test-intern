using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamaraFollow : MonoBehaviour
{

    [SerializeField] Transform m_TagerTranform;

    [SerializeField] Vector3 m_Offset;

    private void Update()
    {
        transform.position = m_TagerTranform.position + m_Offset; 
    }

}
