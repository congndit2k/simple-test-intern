using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UIManager : MonoBehaviour
{
    [SerializeField] private TMP_Text m_pointTxt;
    [SerializeField] GameObject GameOverPn;
    [SerializeField] GameObject YouWonPn;


    private void OnEnable()
    {
        Player.PointUpdated += OnPointUpdated;
    }

    private void OnDisable()
    {
        Player.PointUpdated -= OnPointUpdated;
    }

    private void Awake()
    {
        GameOverPn.SetActive(false);
        YouWonPn.SetActive(false);
    }

    void OnPointUpdated(Player.PlayerData data)
    {
        m_pointTxt.text = data.point+"";
    }

    public void GameOver()
    {
        GameOverPn.SetActive(true);
    }

    public void YouWon()
    {
        YouWonPn.SetActive(true);
    }


    public void Restart()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void MainMenu()
    {
        SceneManager.LoadScene("Menu");
    }
}
