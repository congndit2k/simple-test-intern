using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Point : MonoBehaviour
{
    [System.Serializable]
    public enum PointColor
    {
        Red, Green, Blue
    }

    [System.Serializable]
    public struct PointData
    {
        public PointColor color;
    }

    public PointData m_data;

}
